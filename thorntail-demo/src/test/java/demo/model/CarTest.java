package demo.model;

import demo.model.Car;
import demo.model.TestCarObject;
import org.fest.assertions.Assertions;
import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;

/**
 * @author Ken Finnigan
 */
public class CarTest {

    @Test
    public void carsAreEqual() throws Exception {
        Person person1 = createPerson(3, "gen. Vaclav Moravek", "moravek@acr.cz", 96);
        Car car1 = createCar(1, "Volvo", "V60", "KLM 63-51", person1);
        Car car2 = createCar(1, "Volvo", "V60", "KLM 63-51", person1);

        Assertions.assertThat(car1).isEqualTo(car2);
        assertThat(car1.equals(car2)).isTrue();
        assertThat(car1.hashCode()).isEqualTo(car2.hashCode());
    }

    @Test
    public void carsAreNotEqual() throws Exception {
        Person person1 = createPerson(3, "gen. Vaclav Moravek", "moravek@acr.cz", 96);
        Car car1 = createCar(2, "Volvo", "V60", "KLM 63-51", person1);
        Car car2 = createCar(1, "Volvo", "V60", "KLM 63-51", person1);

        Assertions.assertThat(car1).isNotEqualTo(car2);
        assertThat(car1.equals(car2)).isFalse();
        assertThat(car1.hashCode()).isNotEqualTo(car2.hashCode());
    }

    @Test
    public void CarModification() throws Exception {
        Person person1 = createPerson(3, "gen. Vaclav Moravek", "moravek@acr.cz", 96);
        Car car1 = createCar(1, "Volvo", "V60", "KLM 63-51", person1);
        Car car2 = createCar(1, "Volvo", "V60", "KLM 63-51", person1);

        Assertions.assertThat(car1).isEqualTo(car2);
        assertThat(car1.equals(car2)).isTrue();
        assertThat(car1.hashCode()).isEqualTo(car2.hashCode());

        car1.setLicencePlate("KLM 64-51");

        Assertions.assertThat(car1).isNotEqualTo(car2);
        assertThat(car1.equals(car2)).isFalse();
        assertThat(car1.hashCode()).isNotEqualTo(car2.hashCode());
    }
    

    private Car createCar(Integer id, String brand, String type, String licencePlate, Person person) {
        return new TestCarObject(id, brand, type, licencePlate, person);
    }

    private Person createPerson(Integer id, String name, String email, Integer age) {
        return new TestPersonObject(id, name, email, age);
    }
}
