package client;

import au.com.dius.pact.consumer.ConsumerPactTestMk2;
import au.com.dius.pact.consumer.MockServer;
import au.com.dius.pact.consumer.dsl.PactDslWithProvider;
import au.com.dius.pact.model.PactSpecVersion;
import au.com.dius.pact.model.RequestResponsePact;
import client.model.Car;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.fest.assertions.Assertions;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;

import static org.fest.assertions.Assertions.assertThat;

/**
 * @author Ken Finnigan
 */
public class ConsumerPactTest extends ConsumerPactTestMk2 {
    private Car createCar(Integer id) {
        Car car = new TestCarObject(id);
        car.setBrand("Volvo");
        car.setType("XC60");
        car.setLicencePlate("ZL 96-98");
        car.setOwner(new TestPersonObject(1, "gen. Josef Balaban", "balaban@acr.cz", 98));
        return car;
    }

    @Override
    protected RequestResponsePact createPact(PactDslWithProvider builder) {
        Car car = createCar(100);

        ObjectMapper mapper = new ObjectMapper()
                .registerModule(new JavaTimeModule());

        try {
            return builder
                    .uponReceiving("Retrieve a car")
                        .path("/car/100")
                        .method("GET")
                    .willRespondWith()
                        .status(200)
                        .body(mapper.writeValueAsString(car))
                    .toPact();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }


        //ALL

//        ArrayList<Category> collection = new ArrayList<>();
//        collection.add(top);
//        collection.add(transport);
//        collection.add(autos);
//        collection.add(cars);
//        collection.add(toyotas);



//        try {
//            return builder
//                    .uponReceiving("Retrieve a category collection")
//                    .path("/admin/category")
//                    .method("GET")
//                    .willRespondWith()
//                    .status(200)
//                    .body(mapper.writeValueAsString(toyotas))
//                    .toPact();
//        } catch (JsonProcessingException e) {
//            e.printStackTrace();
//        }

//        //UPDATE
//
//        toyotas.setName("Toyotas Super Cars");
//
//        try {
//            builder
//                    .uponReceiving("Update the category")
//                    .path("/admin/category")
//                    .method("PUT")
//                    .body(toyotas.toString())
//                    .willRespondWith()
//                    .status(200)
//                    .body(mapper.writeValueAsString(collection))
//                    .toPact();
//        } catch (JsonProcessingException e) {
//            e.printStackTrace();
//        }
//
//
//        try {
//            return builder
//                    .uponReceiving("Create the category")
//                    .path("/admin/category")
//                    .method("POST")
//                    .body(toyotas.toString())
//                    .willRespondWith()
//                    .status(200)
//                    .body(mapper.writeValueAsString(collection))
//                    .toPact();
//        } catch (JsonProcessingException e) {
//            e.printStackTrace();
//        }




        return null;
    }

    @Override
    protected String providerName() {
        return "admin_service_provider";
    }

    @Override
    protected String consumerName() {
        return "admin_client_consumer";
    }

    @Override
    protected PactSpecVersion getSpecificationVersion() {
        return PactSpecVersion.V3;
    }

    @Override
    protected void runTest(MockServer mockServer) throws IOException {
        DemoClient demoClient = new DemoClient(mockServer.getUrl());
        Car car = demoClient.getCar(100);

        Assertions.assertThat(car).isNotNull();
        assertThat(car.getId()).isEqualTo(100);


        //ALL
//        Collection<Category> all = adminClient.getAllCategories();
//        assertThat(all.size()).isEqualTo(5);

//        //Update
//        Category cars = createCategory(1009, "Cars");
//        Category toyotas = createCategory(1015, "Toyota Cars");
//        toyotas.setParent(cars);
//        Response r = new AdminClient(mockServer.getUrl()).updateCategory(toyotas);
//        assertThat(r.returnResponse().getStatusLine().getStatusCode()).isEqualTo(200);
//
//        //Create
//        toyotas = createCategory(1015, "Toyota Cars");
//        toyotas.setParent(cars);
//        r = new AdminClient(mockServer.getUrl()).addCategory(toyotas);
//        assertThat(r.returnResponse().getStatusLine().getStatusCode()).isEqualTo(200);
//        cat = new AdminClient(mockServer.getUrl()).getCategory(1015);
//        assertThat(cat.getName()).isEqualTo("Toyota Cars");
//        assertThat(cat.getHeader()).isEqualTo("header");
//        assertThat(cat.getImagePath()).isEqualTo("n/a");
//        assertThat(cat.isVisible()).isTrue();
//        assertThat(cat.getParent()).isNotNull();
//        assertThat(cat.getParent().getId()).isEqualTo(1009);
//
//        //Delete
//        r = new AdminClient(mockServer.getUrl()).removeCategory(1009);
//        assertThat(r.returnResponse().getStatusLine().getStatusCode()).isEqualTo(200);
//        cat = new AdminClient(mockServer.getUrl()).getCategory(1015);
//        assertThat(cat).isEqualTo(null);
    }


}
